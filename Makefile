# --------------------------------------------------------------------
# Copyright (c) 2019 LINKIT, The Netherlands. All Rights Reserved.
# Author(s): Anthony Potappel
# 
# This software may be modified and distributed under the terms of the
# MIT license. See the LICENSE file for details.
# --------------------------------------------------------------------
# all our targets are phony (no files to check).
.PHONY: install help build rebuild down test  apprestar 

# suppress makes own output
#.SILENT:

# shell is the first target. So instead of: make shell cmd="whoami", we can type: make cmd="whoami".
# more examples: make shell cmd="whoami && env", make shell cmd="echo hello container space".
# leave the double quotes to prevent commands overflowing in makefile (things like && would break)
# special chars: '',"",|,&&,||,*,^,[], should all work. Except "$" and "`", if someone knows how, please let me know!).
# escaping (\) does work on most chars, except double quotes (if someone knows how, please let me know)
# i.e. works on most cases. For everything else perhaps more useful to upload a script and execute that.

# Regular Makefile part for buildpypi itself
help:
	@echo ''
	@echo '  build    	build app docker --image-- '
	@echo '  rebuild  	rebuild app docker --image-- '
	@echo '  test     	test End to End '
	@echo '  exec           exec cmd in  node js server make exec "cmd"  '
	@echo '  apprestart   	restart server.js '
	@echo '  down           down stack services '
	@echo '  install        Deploy project '
rebuild:
	# force a rebuild by passing --no-cache
	docker-compose build --no-cache 

appresart:
	# restart server.js 
	docker restart app 

install:
	# Deploy project
	docker-compose up -d 

build:
	# only build the container. Note, docker does this also if you apply other targets.
	docker-compose build 

down:
	# down stack services 
	docker-compose down


test:
	# Start test 
	docker run --rm  -it --network host -v   ${PWD}/test:/app -w /app   everpeace/curl-jq  bash ./test.sh
exec: 
        # exec cmd in  node js server make exec "cmd"
	docker exec -it app bash
