module.exports = (app) => {
    const notes = require('../controllers/noteAppController.js');
    const authnote  = require('../middleware/auth');
    // Create a new Note
    app.post('/api/notes', authnote, notes.create,);

    // Retrieve all Notes
    app.get('/api/notes', authnote, notes.findAll);

    // Retrieve a single Note with noteId
    app.get('/api/notes/:noteId', authnote, notes.findOne);

    // Update a Note with noteId
    app.put('/api/notes/:noteId', authnote, notes.update);

    // Delete a Note with noteId
    app.delete('/api/notes/:noteId', authnote, notes.delete);
}
