module.exports = (app) => {
    const users = require('../controllers/UserController.js');

    // Create a new usere
    app.post('/api/auth/signup', users.signup);
    // login with user 
    app.post('/api/auth/login', users.login);
}
