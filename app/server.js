// create express app
const express = require('express');
const app = express();

// Connect to Database
const mongoose = require('mongoose');
const db = require ('./db');

// parse requests of content-type - application/x-www-form-urlencoded
var bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: true }));
// using bodyParser to parse JSON bodies into JS objects
app.use(bodyParser.json());

// adding Helmet to enhance your API's security
const helmet = require('helmet');
app.use(helmet());

// enabling CORS for all requests
const cors = require('cors');
app.use(cors());

// adding morgan to log HTTP requests
const morgan = require('morgan');
app.use(morgan('combined'));

// Call the routes 
require('./routes/routes.js')(app); // routes notes
require('./routes/routesuser.js')(app); // routes users

const PORT = 8080;
const HOST = '0.0.0.0';
var server = app.listen(PORT, HOST, function () {
console.log('Server is running');
});
