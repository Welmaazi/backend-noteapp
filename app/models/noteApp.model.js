var mongoose = require('mongoose');  
// Pour modéliser les données, le framework mongoose utilise des "schémas" ; nous créons donc un modèle de données :
var NoteSchema = new mongoose.Schema({  
  title: String,
  content: String
}, {
   timestamps: true
});
module.exports = mongoose.model('Note' , NoteSchema);
