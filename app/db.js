const mongoose = require('mongoose');
const options = { server: { socketOptions: { keepAlive: 300000, connectTimeoutMS: 30000 } },
replset: { socketOptions: { keepAlive: 300000, connectTimeoutMS : 30000 } } };
//URL de notre base
//const urlmongo = "mongodb://MONGO_INITDB_ROOT_USERNAME:MONGO_INITDB_ROOT_PASSWORD@mongo:27017/admin"; // replace MONGO_INITDB_ROOT_USERNAME and   MONGO_INITDB_ROOT_PASSWORD with username & password database
const urlmongo = "mongodb://admin:test@mongo:27017/admin"; //default
// Nous connectons l'API à notre base de données
mongoose.connect(urlmongo, options);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'Erreur lors de la connexion'));
db.once('open', function (){
    console.log("Connexion à la base OK");
});